﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEMO_AdvancedCameraControls : MonoBehaviour
{
    public Transform player;
    public Transform camera;
    public Vector3 rotateLimits;
    public float rotateSpeed = 5;

    private Vector3 offset;
    private Transform _cameraParent;


    // Start is called before the first frame update
    void Start()
    {
        _cameraParent = transform;
        offset = camera.position - player.position;
        camera.position = offset;
    }

    // Update is called once per frame
    void Update()
    {
        _cameraParent.position = player.position;
        
        float cameraRotateHorizontal = Input.GetAxis("CameraHorizontal");
        float rotationAmount = cameraRotateHorizontal * rotateSpeed;

        if (_cameraParent.eulerAngles.y < rotateLimits.y && _cameraParent.eulerAngles.y > -rotateLimits.y)
        {
            _cameraParent.Rotate(new Vector3(0, 1, 0), rotationAmount, Space.World);
        }
        if(_cameraParent.eulerAngles.y <= -rotateLimits.y)
        {
            _cameraParent.eulerAngles = new Vector3(0, rotateLimits.y + 0.01f, 0);
        }
        if (_cameraParent.eulerAngles.y >= rotateLimits.y)
        {
            _cameraParent.eulerAngles = new Vector3(0, rotateLimits.y - 0.01f, 0);
        }
    }
}
    